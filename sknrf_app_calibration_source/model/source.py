"""
    =====================================
    Source Calibration Models
    =====================================

    This module defines the calibration models for the source calibrations.

    See Also
    ----------
    sknrf.view.desktop.calibration.receiver.base.AbstractReceiverModel, sknrf.model.base.AbstractModel
"""
import logging

import torch as th

from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.settings import Settings
from sknrf.device.signal import tf, ff
from sknrf.model.calibration.base import AbstractCalibrationModel
from sknrf_transform_ff_system.model.system import CascadeTransform
from sknrf.enums.device import Instrument
from sknrf.utilities.numeric import Info
from sknrf.utilities.rf import g2z, a2t, t2a, t2tp, tp2t, viz2baz, n2t

logger = logging.getLogger(__name__)


class GainModel(AbstractCalibrationModel):
    """A calibration model for vector calibrations.

        See Also
        ----------
        AbstractReceiverModel
    """

    def __init__(self, instrument_flags=Instrument.RFSOURCE):
        super(GainModel, self).__init__(instrument_flags=instrument_flags)
        self.measurement_type = "SS"
        self._src = None

    def __getstate__(self, state={}):
        state = super(GainModel, self).__getstate__(state=state)
        # ### Manually save selected object PROPERTIES here ###
        state["measurement_type"] = self.measurement_type
        state["_src"] = self._src
        return state

    def __setstate__(self, state):
        super(GainModel, self).__setstate__(state)
        # ### Manually load saved object ATTRIBUTES and PROPERTIES here ###
        self.measurement_type = state["measurement_type"]
        self._src = state["_src"]

    def __info__(self):
        """ Initializes the display information of a device and stores information in self.info.
        """
        super(GainModel, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["measurement_type"] = Info("measurement type", read=False, write=False, check=False)

    @property
    def port_indices(self):
        return self._port_indicies

    @port_indices.setter
    def port_indices(self, port_indices):
        self._port_indicies = port_indices
        if len(port_indices) > 0:
            port = self.device_model().ports[port_indices[0]]
            lf_v = tf.pk(port.lfsource.v)
            rf_a = tf.pk(port.rfsource.a_p)
            self._src = th.cat((lf_v, rf_a), dim=-1).repeat((Settings().t_points, 1))

    def calculate(self):
        port_index = self.port_indices[0]

        page_name = "Load"
        port_nums = "_%d" % (port_index,)
        num_fund = Settings().t_points
        freq = ff.freq()

        dataset = self.datagroup_model()["GainModel"].dataset(page_name + port_nums)
        v = n2t(dataset.__getattr__("v_%d" % (port_index,))[...])
        i = n2t(dataset.__getattr__("i_%d" % (port_index,))[...])
        z = n2t(dataset.__getattr__("z_%d" % (port_index,))[...])
        b = n2t(dataset.__getattr__("b_%d" % (port_index,))[...])
        a = n2t(dataset.__getattr__("a_%d" % (port_index,))[...])
        g = n2t(dataset.__getattr__("g_%d" % (port_index,))[...])
        b[..., 0:1], a[..., 0:1], g[..., 0:1] = viz2baz(v, i, z)
        b, a, g = tf.ff(b), tf.ff(a), tf.ff(g)
        z = g2z(g)[0]

        s_shape = th.prod(th.as_tensor(dataset.shape[-2:])), dataset.shape[-5], dataset.shape[-5]
        meas = th.zeros(s_shape, dtype=si_dtype_map[SI.B])
        z_port = th.zeros(s_shape, dtype=si_dtype_map[SI.Z])
        time_index = slice(1, 2, 1) if num_fund == 2 else slice(None)
        for fund_index in range(Settings().t_points):
            meas[num_fund*0:num_fund, 0, 0] = v[..., 0, fund_index, time_index, 0]
            z_port[num_fund*0:num_fund, 0, 0] = z[..., 0, fund_index, time_index, 0]
        for harm_index in range(1, Settings().f_points):
            for fund_index in range(Settings().t_points):
                meas[num_fund * harm_index:num_fund*(harm_index + 1), 0, 0] = a[..., harm_index, fund_index, time_index, harm_index]
                z_port[num_fund * harm_index:num_fund*(harm_index + 1), 0, 0] = z[..., harm_index, fund_index, time_index, harm_index]
        src = self._src.reshape(-1, 1, 1)
        z_port = z_port.flatten()

        # Adapter
        abcd = th.eye(2, dtype=meas.dtype).repeat(freq.shape[0], 1, 1)
        tp = t2tp(a2t(th.inverse(abcd))[0], z_port, z_port)[0]

        gain = th.zeros((Settings().t_points*Settings().f_points, 1, 1), dtype=meas.dtype)
        gain[0:num_fund] = abcd[0:num_fund, 0::2, 0::2].reshape(-1, 1, 1)*meas[0:num_fund]/src[0:num_fund]
        gain[num_fund:] = tp[num_fund:, 1::2, 1::2].reshape(-1, 1, 1)*meas[num_fund:]/src[num_fund:]

        tp = th.zeros((Settings().t_points*Settings().f_points, 2, 2), dtype=gain.dtype)
        tp[num_fund:, 0::2, 0::2] = th.inverse(gain[num_fund:, 0::2, 0::2])
        tp[num_fund:, 1::2, 1::2] = gain[num_fund:, 0::2, 0::2]
        abcd = t2a(tp2t(tp, z_port, z_port)[0])[0]
        abcd[0:num_fund, 0::2, 0::2] = th.inverse(gain[0:num_fund, 0::2, 0::2])
        abcd[0:num_fund, 1::2, 1::2] = gain[0:num_fund, 0::2, 0::2]
        self.calibration = abcd

    def apply_cal(self):
        name = "src cal"
        transforms = self.device_model().transforms
        port_list = self.port_indices
        abcd = self.calibration
        transform = CascadeTransform(name, port_list, instrument_flags=self.instrument_flags,
                                     abcd=abcd)
        transforms.append(name, transform)
