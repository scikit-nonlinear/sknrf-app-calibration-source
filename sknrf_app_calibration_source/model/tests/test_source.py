import os
import unittest

from sknrf.settings import Settings
from sknrf_app_calibration_source.model.source import GainModel
from sknrf.model.calibration.tests.test_calibration import CalibrationMethodTests


class TestSourceCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = GainModel
    cal_port_indices = [1]
    cal_measurements = [["Load", "_1", "Load"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testing.cal"))
    calkit_dg = "GainModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    @classmethod
    def setUpClass(cls):
        super(TestSourceCalibrationMethod, cls).setUpClass()

    def test_measurement(self):
        super(TestSourceCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestSourceCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestSourceCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        pass

    def test_calculate_rf(self):
        super(TestSourceCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestSourceCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestSourceCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestSourceCalibrationMethod, self).test_apply()


def vector_calibration_test_suite():
    test_suite = unittest.TestSuite()

    test_suite.addTest(unittest.makeSuite(TestSourceCalibrationMethod))

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(vector_calibration_test_suite())

