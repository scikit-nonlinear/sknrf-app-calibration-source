import numpy as np
from PySide2.QtGui import QIcon

from sknrf.device.signal import tf
from sknrf.device.instrument import rfztuner
from sknrf.device.instrument.rfreceiver import NoRFReceiver
from sknrf.device.instrument.rfsource import NoRFSource
from sknrf_app_calibration_source.model.source import GainModel
from sknrf.enums.device import Instrument
from sknrf.view.desktop.calibration.wizard.base import AbstractCalibrationWizard
from sknrf.view.desktop.calibration.wizard.base import AbstractContentPage, AbstractConclusionPage
from sknrf.view.desktop.calibration.wizard.base import AbstractPortPage, AbstractInstrumentPage, \
    AbstractRequirementsPage
from sknrf.utilities.patterns import export


class GainPortPage(AbstractPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class GainInstrumentPage(AbstractInstrumentPage):

    def __init__(self, _parent):
        super(GainInstrumentPage, self).__init__(_parent)
        self.add_instrument(Instrument.RFSOURCE)


class GainRequirementsPage(AbstractRequirementsPage):

    def __init__(self, _parent):
        super(GainRequirementsPage, self).__init__(_parent)
        self.add_requirement("LF Source is OFF for all ports.")
        self.add_requirement("RF Source is NOT connected for calibration port.")
        self.add_requirement("RF Source is connected for measurement port.")
        self.add_requirement("RF Source is ON for measurement port.")
        self.add_requirement("RF Source.a_p > min for measurement port.")
        self.add_requirement("RF Receiver is connected for measurement port.")

        self.add_recommendation("RF ZTuner.z_set == 50 Ohm for all ports.")
        self.connect_signals()

    def check_requirements(self):
        super(GainRequirementsPage, self).check_requirements()
        state = [True]*len(self.requirementsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                state[1] = state[1] and type(port.rfsource) is NoRFSource
            else: # Measurement Port
                state[2] = state[2] and type(port.rfsource) is not NoRFSource
                state[3] = state[3] and port.rfsource.on
                info = port.rfsource.info
                min, abs_tol = info["a_p"].min, info["a_p"].abs_tol
                state[4] = state[4] and np.all(np.abs(tf.pk(port.rfsource.a_p)) > min + abs_tol)
                state[5] = state[5] and type(port.rfreceiver) is not NoRFReceiver
            # All Ports
            port.lfsource.on = False
            state[0] = state[0] and not port.lfsource.on
        for index, value in enumerate(state):
            self.requirementsCheckBoxList[index].setChecked(value)

    def check_recommendations(self):
        super(GainRequirementsPage, self).check_recommendations()
        state = [True] * len(self.recommendationsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                pass
            else: # Measurement Port
                pass
            # All Ports
            info = port.rfztuner.info
            rel_tol, abs_tol = info["z_set"].rel_tol, info["z_set"].abs_tol
            state[0] = state[0] and np.allclose(tf.avg(port.rfztuner.z_set), 50.0, rel_tol, abs_tol)
        for index, value in enumerate(state):
            self.recommendationsCheckBoxList[index].setChecked(value)


class GainContentPage(AbstractContentPage):
    pass


class GainConclusionPage(AbstractConclusionPage):
    pass


class GainWizard(AbstractCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(GainWizard, self).__init__(parent=parent, calkit_package=rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = GainModel()
        calkit_model = model.device_model().ports[0].rfreceiver
        self.set_model(model, calkit_model)

        self.addPage(GainPortPage(self))
        self.addPage(GainInstrumentPage(self))
        self.addPage(GainRequirementsPage(self))
        self.addPage(GainConclusionPage(self))

    def initialize_content_pages(self):
        super(GainWizard, self).initialize_content_pages()
        pages = list()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "Load"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "Load"]
            pages.append(GainContentPage(self, "Load_%d" % (port_num,), contents, optional=False))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")